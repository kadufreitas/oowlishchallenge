import React, { Component } from "react";
import "./App.css";
import "../../css/button.css";
import HoursRegister from "../hours_register/hoursRegister";
import withFirebaseAuth from "react-with-firebase-auth";
import * as firebase from "firebase/app";
import "firebase/auth";
import firebaseConfig from "../../firebaseConfig";
import API from "../../utils/API";

const firebaseApp = firebase.initializeApp(firebaseConfig);

class App extends Component {
  constructor() {
    super();
    this.state = {
      hoursRegister: {
        registerTime: {
          arrival: new Date(),
          exit: new Date(),
          registerType: "work"
        },
        registerList: {
          registers: []
        },
        workControl: {
          missing: 0,
          extra: 0
        }
      },
      hoursControl: {},
      user: null
    };
  }

  componentDidMount() {
    this.getDataUser();
  }

  componentDidUpdate() {
    if (this.props.user) {
      if (!localStorage.getItem("userId")) {
        localStorage.setItem("userId", this.props.user.uid);
        this.getDataUser();
      }
    }
  }

  handleChangeDateTime = (date, type) => {
    console.log(date);
    this.handleSetState("hoursRegister", "registerTime", type, date);
  };

  handleOnChange = e => {
    const { name, value } = e.target;
    this.handleSetState("hoursRegister", "registerTime", name, value);
  };

  handleSetState = (parent, children, name, value) => {
    this.setState(currentSate => ({
      [parent]: {
        ...currentSate[parent],
        [children]: {
          ...currentSate[parent][children],
          [name]: value
        }
      }
    }));
  };

  handleRegisterTime = async () => {
    const userId = this.props.user.uid;
    const registerTime = this.state.hoursRegister.registerTime;
    const registerList = this.state.hoursRegister.registerList;
    const newRegister = {
      typeRegister: registerTime.registerType,
      arrivalHour: registerTime.arrival.toString(),
      exitHour: registerTime.exit.toString(),
      totalHours: this.calcTotalHours(registerTime.arrival, registerTime.exit)
    };

    const newArr = [...registerList.registers];
    newArr.push(newRegister);

    this.handleSetState("hoursRegister", "registerTime", "arrival", "");
    this.handleSetState("hoursRegister", "registerTime", "exit", "");
    // this.handleSetState("hoursRegister", "registerList", "registers", newArr);

    try {
      await API.post(`registers/${userId}.json`, newRegister);
      this.getDataUser();
    } catch (e) {
      console.log(`😱 Axios request failed: ${e}`);
    }
  };

  //Utils
  calcTotalHours = (arrival, exit) => {
    return exit.getTime() - arrival.getTime();
  };

  getDataUser = async () => {
    const userId = localStorage.getItem("userId") || null;
    if (userId) {
      try {
        let userData = await API.get(`registers/${userId}.json`);
        const newArr = Object.keys(userData.data).map(key => {
          return userData.data[key];
        });

        const registerToday = this.getTodayRegister(newArr);
        const hoursCounted = this.calcWorkHours(registerToday);

        this.handleSetState(
          "hoursRegister",
          "registerList",
          "registers",
          registerToday
        );
        this.handleSetState(
          "hoursRegister",
          "workControl",
          "missing",
          hoursCounted.missing
        );
        this.handleSetState(
          "hoursRegister",
          "workControl",
          "extra",
          hoursCounted.extra
        );
        //this.setState()
      } catch (e) {
        console.log(`😱 Axios request failed: ${e}`);
      }
    }
  };

  getTodayRegister = registers => {
    return registers.filter(item => {
      const toDay = new Date();
      const currentDate = new Date(item.arrivalHour);
      return (
        toDay.getFullYear() === currentDate.getFullYear() &&
        toDay.getMonth() === currentDate.getMonth() &&
        toDay.getDate() === currentDate.getDate()
      );
    });
  };

  convertMilisecToHours = duration => {
    let hours = Math.floor((duration / (1000 * 60 * 60)) % 24);
    let minutes = Math.floor((duration / (1000 * 60)) % 60);
    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    return `${hours}:${minutes}`;
  };

  calcWorkHours = listRegisters => {
    const totalHours = listRegisters
      .filter(item => {
        return item.typeRegister === "work";
      })
      .reduce((acc, current) => {
        console.log(Number(current.totalHours));
        return acc + Number(current.totalHours);
      }, 0);
    const workControl = {
      missing: 0,
      extra: 0
    };

    const diff = 28800000 - totalHours; //28800000 = 8h
    diff > 0
      ? (workControl.missing = this.convertMilisecToHours(diff))
      : (workControl.extra = this.convertMilisecToHours(Math.abs(diff)));

    return workControl;
  };

  clearUser = () => {
    localStorage.removeItem("userId");
  };

  render() {
    const { user, signOut, signInWithGoogle } = this.props;
    return (
      <div className="App clearfix fit mxn2">
        <div className="col-7 px2 mx-auto">
          <header className="flex justify-end">
            {user ? <p>Hello, {user.displayName}</p> : <p>Please sign in.</p>}

            {user ? (
              <button
                className="demo-button mdc-button mdc-button--unelevated demo-button-shaped mdc-ripple-upgraded"
                onClick={() => {
                  this.clearUser();
                  signOut();
                }}
              >
                Sign out
              </button>
            ) : (
              <button
                className="demo-button mdc-button mdc-button--unelevated demo-button-shaped mdc-ripple-upgraded"
                onClick={signInWithGoogle}
              >
                Sign in with Google
              </button>
            )}
          </header>
          {!!user && (
            <HoursRegister
              data={this.state.hoursRegister}
              handleOnChange={this.handleOnChange}
              handleChangeDateTime={this.handleChangeDateTime}
              handleRegisterTime={this.handleRegisterTime}
            />
          )}
        </div>
      </div>
    );
  }
}

const firebaseAppAuth = firebaseApp.auth();

const providers = {
  googleProvider: new firebase.auth.GoogleAuthProvider()
};

export default withFirebaseAuth({
  providers,
  firebaseAppAuth
})(App);
