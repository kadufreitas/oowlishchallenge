import React from "react";
import ReactDOM from "react-dom";
import HoursControl from "./hoursControl";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<HoursControl />, div);
  ReactDOM.unmountComponentAtNode(div);
});
