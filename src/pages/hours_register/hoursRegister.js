import React from "react";
import "./hoursRegister.css";
import TimeRegister from "../../components/time_register/timeRegister";
import ListRegister from "../../components/list_register/listRegister";

const HoursRegister = ({
  data,
  handleOnChange,
  handleRegisterTime,
  handleChangeDateTime
}) => {
  return (
    <div className="HoursRegister">
      <h1>HoursRegister</h1>
      <h5>Today {new Date().toDateString()}</h5>
      <TimeRegister
        registerTime={data.registerTime}
        handleOnChange={handleOnChange}
        handleChangeDateTime={handleChangeDateTime}
        handleRegisterTime={handleRegisterTime}
        workControl={data.workControl}
      />
      <ListRegister registerList={data.registerList} />
    </div>
  );
};
export default HoursRegister;
