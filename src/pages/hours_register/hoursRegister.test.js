import React from "react";
import HoursRegister from "./hoursRegister";
import { shallow, mount, render } from "enzyme";

it("renders without crashing", () => {
  const props = {
    hoursRegister: {
      registerTime: {
        arrival: new Date(),
        exit: new Date(),
        registerType: "work"
      },
      registerList: {
        registers: []
      }
    },
    handleOnChange: () => {},
    handleChangeDateTime: () => {},
    handleRegisterTime: () => {}
  };
  const wrapper = shallow(
    <HoursRegister
      data={props.hoursRegister}
      handleOnChange={props.handleOnChange}
      handleChangeDateTime={props.handleChangeDateTime}
      handleRegisterTime={props.handleRegisterTime}
    />
  );
  expect(wrapper.exists()).toBeTruthy();
});
