import React from "react";
import "./listRegister.css";
import "../../css/Card.css";
import ListItem from "../../template/listItem";

const ListRegister = ({ registerList }) => {
  const mapListItems = () => {
    return registerList.registers.map((item, index) => (
      <ListItem data-test="listItem" key={index} {...item} />
    ));
  };

  return !!registerList.registers.length ? (
    <div className="ListRegister mt2 p2 mdc-card">
      <table className="table clearfix mx-auto" style={{ width: "100%" }}>
        <tbody>
          <tr>
            <th className="row-header">Type</th>
            <th className="row-header">Arrival Hour</th>
            <th className="row-header">Exit Hour</th>
            <th className="row-header">Total Hours</th>
          </tr>
        </tbody>
        <tbody>{mapListItems()}</tbody>
      </table>
    </div>
  ) : (
    <p>Ainda não há nenhum registro.</p>
  );
};

export default ListRegister;
