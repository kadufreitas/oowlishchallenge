import React from "react";
import ListRegister from "./listRegister";
import { shallow } from "enzyme";

const props = {
  registerList: {
    registers: [
      {
        typeRegister: "work",
        arrivalHour: "08:00",
        exitHour: "12:00",
        totalHours: 4
      },
      {
        typeRegister: "break",
        arrivalHour: "12:00",
        exitHour: "14:00",
        totalHours: 2
      },
      {
        typeRegister: "work",
        arrivalHour: "14:00",
        exitHour: "18:00",
        totalHours: 4
      }
    ]
  }
};
it("should render without crashing", () => {
  const wrapper = shallow(<ListRegister registerList={props.registerList} />);
  expect(wrapper.exists()).toBeTruthy();
});

it("should render without crashing", () => {
  const wrapper = shallow(<ListRegister registerList={props.registerList} />);
  expect(wrapper.find("[data-test='listItem']")).toHaveLength(
    props.registerList.registers.length
  );
});
