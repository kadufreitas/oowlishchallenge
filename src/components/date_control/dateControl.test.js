import React from "react";
import ReactDOM from "react-dom";
import DateControl from "./dateControl";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<DateControl />, div);
  ReactDOM.unmountComponentAtNode(div);
});
