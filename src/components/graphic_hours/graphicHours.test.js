import React from "react";
import ReactDOM from "react-dom";
import GraphicHours from "./graphicHours";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<GraphicHours />, div);
  ReactDOM.unmountComponentAtNode(div);
});
