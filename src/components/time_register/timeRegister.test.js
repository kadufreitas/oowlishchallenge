import React from "react";
import TimeRegister from "./timeRegister";
import { shallow, mount } from "enzyme";

const props = {
  registerTime: {
    arrival: new Date("2019-07-27T20:25:45.780Z"),
    exit: new Date("2019-07-27T22:25:45.780Z"),
    registerType: "work"
  },
  workControl: {
    missing: "00:15",
    extra: 0
  },
  handleOnChange: () => {},
  handleRegisterTime: () => {},
  handleChangeDateTime: () => {}
};

it("should render without crashing", () => {
  const wrapper = mount(
    <TimeRegister
      registerTime={props.registerTime}
      handleOnChange={props.handleOnChange}
      handleChangeDateTime={props.handleChangeDateTime}
      handleRegisterTime={props.handleRegisterTime}
      workControl={props.workControl}
    />
  );
  expect(wrapper.exists()).toBeTruthy();
});

it("should render children elements", () => {
  const wrapper = mount(
    <TimeRegister
      registerTime={props.registerTime}
      handleOnChange={props.handleOnChange}
      handleChangeDateTime={props.handleChangeDateTime}
      handleRegisterTime={props.handleRegisterTime}
      workControl={props.workControl}
    />
  );
  expect(wrapper.find("[data-test='registerType']").props().value).toBe("work");
  expect(
    wrapper
      .find("[data-test='inputTime']")
      .at(0)
      .props().selected
  ).toEqual(new Date("2019-07-27T20:25:45.780Z"));
  expect(
    wrapper
      .find("[data-test='inputTime']")
      .at(1)
      .props().selected
  ).toEqual(new Date("2019-07-27T22:25:45.780Z"));
});

//Still don't work

// it("should render 'break' before simulate change select", () => {
//   const wrapper = mount(
//     <TimeRegister
//       registerTime={props.registerTime}
//       handleOnChange={props.handleOnChange}
//       handleChangeDateTime={props.handleChangeDateTime}
//       handleRegisterTime={props.handleRegisterTime}
//     />
//   );
//   const select = wrapper.find("[data-test='registerType']");
//   select.prop("onChange")({
//     target: {
//       name: "registerType",
//       value: "break"
//     }
//   });
//   console.log(wrapper.debug());
//   expect(select.props().value).toEqual("break");
// });
