import React from "react";
import InputTime from "../../template/inputTime";
import Button from "../../template/button";
import "./timeRegister.css";
import "../../css/Card.css";
import "../../css/textField.css";
import TypeRegister from "../../template/typeRegister";

const TimeRegister = ({
  registerTime,
  handleOnChange,
  handleRegisterTime,
  handleChangeDateTime,
  workControl
}) => {
  return (
    <div className="TimeRegister mdc-card">
      <div className="clearfix mxn2 flex items-center p2 justify-center">
        <div className="col px2 ">
          <InputTime
            date={registerTime.arrival}
            handleChangeDateTime={handleChangeDateTime}
            type="arrival"
          />
        </div>
        <div className="col px2">
          <InputTime
            date={registerTime.exit}
            handleChangeDateTime={handleChangeDateTime}
            type="exit"
          />
        </div>
        <div className="col px2">
          <TypeRegister
            value={registerTime.registerType}
            handleOnChange={handleOnChange}
            type="registerType"
          />
        </div>
        <div className="col px2">
          <Button handleRegisterTime={handleRegisterTime} />
        </div>
      </div>
      <div className="clearfix mxn2 flex items-center p2 justify-around demo-card__primary">
        <h5 className="demo-card__subtitle">Total hours to work : 8</h5>
        <h5 className="demo-card__subtitle">
          you need make more <strong>{workControl.missing}</strong> hours
        </h5>
        <h5 className="demo-card__subtitle">
          You have <strong>{workControl.extra}</strong> extra
        </h5>
      </div>
    </div>
  );
};

export default TimeRegister;
