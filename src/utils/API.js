import axios from "axios";

export default axios.create({
  baseURL: "https://oowlish-challenger.firebaseio.com/",
  responseType: "json"
});
