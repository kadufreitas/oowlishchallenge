import React from "react";

const list = ({ typeRegister, arrivalHour, exitHour, totalHours }) => {
  const formatDate = dateStr => {
    let date = new Date(dateStr);
    date.setHours(date.getHours() - 3);
    date = date.toJSON();
    const strSplit = date.split("T");
    return {
      date: strSplit[0],
      time: strSplit[1].substring(0, 8)
    };
  };

  const formatHours = duration => {
    let hours = Math.floor((duration / (1000 * 60 * 60)) % 24);
    let minutes = Math.floor((duration / (1000 * 60)) % 60);
    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    return `${hours}:${minutes}`;
  };

  let arrivalFormated = formatDate(arrivalHour);
  let exitFormated = formatDate(exitHour);
  arrivalFormated = `${arrivalFormated.date} ${arrivalFormated.time}`;
  exitFormated = `${exitFormated.date} ${exitFormated.time}`;

  return (
    <tr>
      <td>{typeRegister}</td>
      <td>{arrivalFormated}</td>
      <td>{exitFormated}</td>
      <td>{formatHours(totalHours)}</td>
    </tr>
  );
};

export default list;
