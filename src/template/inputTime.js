import React from "react";
import PropTypes from "prop-types";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

const inputTime = ({ date, handleChangeDateTime, type }) => (
  // <input
  //   value={value}
  //   onChange={handleOnChange}
  //   placeholder={`${type} e.g: `}
  //   name={type}
  // />
  <div className="inputTime">
    <DatePicker
      data-test="inputTime"
      selected={date}
      onChange={e => handleChangeDateTime(e, type)}
      showTimeSelect
      timeFormat="HH:mm"
      timeIntervals={15}
      dateFormat="MMMM d, yyyy h:mm aa"
      timeCaption="time"
    />
    <div>
      <span className="pr1">{type}</span>
    </div>
  </div>
);

inputTime.propTypes = {
  value: PropTypes.string,
  handleChangeDateTime: PropTypes.func
};

export default inputTime;
