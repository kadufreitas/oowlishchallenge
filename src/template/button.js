import React from "react";
import "../css/button.css";
const button = ({ handleRegisterTime }) => {
  // return <button >Register</button>;
  return (
    <button
      className="demo-button mdc-button mdc-button--unelevated demo-button-shaped mdc-ripple-upgraded"
      onClick={handleRegisterTime}
    >
      <span className="mdc-button__label">save</span>
    </button>
  );
};

button.propTypes = {};

export default button;
