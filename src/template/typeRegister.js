import React from "react";
import PropTypes from "prop-types";
import "../css/textField.css";
const typeRegister = ({ type, value, handleOnChange }) => (
  <div>
    <select
      data-test="registerType"
      value={value}
      onChange={handleOnChange}
      name={type}
    >
      <option value="work">Work</option>
      <option value="break">Break</option>
    </select>
    <div>
      <span className="pr1">register type</span>
    </div>
  </div>
);

typeRegister.propTypes = {
  value: PropTypes.string.isRequired,
  handleOnChange: PropTypes.func.isRequired
};

export default typeRegister;
